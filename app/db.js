const stations = [
  {
    id: 1,
    name: 'Putin FM',
    avatar: 'https://upload.wikimedia.org/wikipedia/en/2/22/Heckert_GNU_white.svg',
    freq: '66,6'
  },
  {
    id: 2,    
    name: 'Dribble FM',
    avatar: 'http://kompozer.net/images/svg/Mozilla_Firefox.svg',
    freq: '101,2'
  },
  {
    id: 3,    
    name: 'Doge FM',
    avatar: 'http://media.vectormagic.com/tutorials/scans/knot_150_result.svg',
    freq: '99,4'
  },
  {
    id: 4,    
    name: 'Ballads FM',
    avatar: 'http://www.freedos.org/images/logos/fdfish-glossy-plain.svg',
    freq: '87,1'
  },
  {
    id: 5,
    name: 'Maximum FM',
    avatar: 'http://snapsvg.io/assets/images/logo.svg',
    freq: '142,2'
  }
]

export default stations