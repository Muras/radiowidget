// Radio controllers, which appers after onClick action in stationsElement.js

import React from 'react'

const radioControls = React.createClass({
  render () {
    return (
      <div className='radio_list--component--avatar'>
        <div className='radio_list--component--avatar-minusBtn'><div className='entypo-minus'></div></div>
        <div className='radio_list--component--avatar-wrap'>
          <img src={this.props.imgSrc} className='radio_list--component--avatar-img'/>
        </div>
        <div className='radio_list--component--avatar-plusBtn'><div className='entypo-plus'></div></div>
      </div>
    )
  }
})

export default radioControls