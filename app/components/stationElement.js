//Code which is responsible for single stations element and controlls rendering
import React from 'react'
//=================================================================//
import RadioControls from './radioControls'

//Here go display every radio in our database (db.js). 
//I had many problems with rendering the list and setting this.state to seperate elements.
//This is the reason why I have decided to make it static (app.js) in order to save time for styling

//btnChangeStatus = This function checks is there any other button clicked. If NOT, 
//id adds unactive class to every element on rendered list (ourList)
//and removes this class from actual button.
//Surelly there are other, easier ways to handle this problem but for now,
// it is all I have figured out.

const ourList = document.getElementsByClassName('radio_list--component')

export class StationElement extends React.Component {
  constructor() {
    super()
    this.state = {
      clicked: false,
      actualStation: ''
    }
    this.handleClick = this.handleClick.bind(this)
  }
  handleClick(entry) {

    this.btnChangeStatus(entry)
    this.setState({ clicked: !this.state.clicked })
    console.log(this.state.textField)
  }
  btnChangeStatus(entry) {
    let i, activeID;
    if (this.state.clicked === false) {
      for (i = 0; i < ourList.length; i++) {
        ourList[i].classList.add('unactiveBtn')
        if (ourList[i].id == this.props.entry.id) {
          ourList[i].classList.remove('unactiveBtn')
          this.state.actualStation = ourList[i].id
        }
      }
    } else {
      for (i = 0; i < ourList.length; i++) {
        ourList[i].classList.remove('unactiveBtn')
      }
    }
  }
  render() {
    return (
      <div className='radio_list--component' id={this.props.entry.id} >
        {this.state.clicked ? <RadioControls imgSrc={this.props.entry.avatar} /> : null }
        <div  className='radio_list--component--clickTab' onClick={() => this.handleClick(this.state) }>
          <div className='radio_list--component--clickTab-stationName' >{this.props.entry.name}</div>
          <div className='radio_list--component--clickTab-freq'>{this.props.entry.freq}</div>
        </div>
      </div>
    )
  }
}
