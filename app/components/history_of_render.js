/*this code is unrelevant for the task, just shows the path I took before*/

import React from 'react'
//=====================================================================================//

class radiosList extends React.Component {
  constructor(entry) {
    super(entry);
    this.state = {
      data: []
    }
    this.handleClick = this.handleClick.bind(this)
  }
  handleClick(station) {
    console.log(this)
    var data = [...this.state.data];
    data[station].clicked = !data[station].clicked;
    this.setState({data});
  }
  render () {
    var self = this;
    console.log(self.state)
     this.props.route.entry.forEach(function(station) {
       self.state.data.push({name: station.name, avatar: station.avatar, freg: station.freq, clicked: false})
       self.setState({data: self.state.data});
     })
     return (
      <div>
        {
          this.state.data.map((station, index) => (
            <div className='radio_list radio_list--component' style={{cursor: 'pointer'}} onClick={() => this.handleClick(station)}>
              <div className='radio_list radio_list--component radio_list--component-station_name'>{station.name}</div>
              <div className='radio_list radio_list--component radio_list--component-freq'>{station.freq}</div>
            </div> 
          ))
        }
      </div>
    )
  }
}

export default radiosList

//=====================================================================================//
class radiosList extends React.Component {
  constructor() {
    super();
    this.state = {
      clicked: false
    }
    this.handleClick = this.handleClick.bind(this)
  }
  handleClick(station) {
    
    this.setState({ clicked: !this.state.clicked })
  }
  render () {
     return (
      <div>
        {
          this.props.stations.map((station, index) => (
            <div className='radio_list radio_list--component' style={{cursor: 'pointer'}} onClick={() => this.handleClick(station.avatar)}>
            {this.state.clicked ? <RadioControls imgSrc={station.avatar}/> : null }
              <div className='radio_list radio_list--component radio_list--component-station_name'>{station.name}</div>
              <div className='radio_list radio_list--component radio_list--component-freq'>{station.freq}</div>
            </div> 
          ))
        }
      </div>
    )
  }
}
    
//=====================================================================================//    
    render () {
    
    let listComponents = this.props.stations.map((station, index) => {
      
     return (
      <div className='radio_list radio_list--component' style={{cursor: 'pointer'}} onClick={() => this.handleClick(station.avatar)}>
      {this.state.clicked ? this.freq : null}
        <div className='radio_list radio_list--component radio_list--component-station_name'>{station.name}</div>
        <div className='radio_list radio_list--component radio_list--component-freq'>{station.freq}</div>
      </div> 
      )}
    )
    return <div className = 'radio_list'>{listComponents}</div>
  }
  
