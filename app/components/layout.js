//MainPage layout
import React from 'react'

const layout = React.createClass({

  render() {
    return (
      <div className='layout'>
        <div className='nav'>
          <div className='nav--backBtn entypo-left-open-big'></div>
          <div className='nav--middleText'>stations</div>
          <div className='nav--powerBtn fontawesome-off'></div>
        </div>
        <div className='radio_list'>

          {this.props.children}

        </div>
        <div className='footer'>
          <div className='footer--staticText'>currently playing</div>
          <div className='footer--currentStation' id='currentStation'>{this.props.station}</div>
        </div>
      </div>
    )
  }
})

export default layout