//Main App file, where we put everything together and render with React
import React from 'react'
import ReactDOM from 'react-dom'
import { Router, Route, hashHistory } from 'react-router'

//============= START: react modules =============//
import db from './db'
import {StationElement} from './components/stationElement'
import Layout from './components/layout'
//============= END: react modules =============//
import style from './sass/main.scss'

/* 
I wanted to make it more complicated, with DB mapping but it took me to much time to figure out how to set different states
while mapping through the array of objects. So I decided to make it static in order to save time and be able to finish task on time.
I would probably be able to achive this.
*/

const App = React.createClass({
  render() {
    return (
      <Layout>
          <StationElement entry={db[0]}/>
          <StationElement entry={db[1]}/>
          <StationElement entry={db[2]}/>
          <StationElement entry={db[3]}/>
          <StationElement entry={db[4]}/>
      </Layout>
    )
  }
})

ReactDOM.render(<App/>, document.getElementById('app'))