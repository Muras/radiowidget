# radioWidget

[LINK CLICK](http://vibs-glasses.com/)

Description

Simple widget with list of 5 different radio stations. As an user you should be able to click on one of the elements 
and then radio controls should apper. At this moment, only clicked title can be hidden. 

TO DO

1. Important features:
  - at the bottom: display current radio station
  - when there is no station clicked => empty footer

2. Additional:
  - scrolling without scrollbars
  - add custom font (Helvetica) from local files (sass problems:( )