var HTMLWebpackPlugin = require('html-webpack-plugin')
var HTMLWebpackPluginConfig = new HTMLWebpackPlugin({
  template: __dirname + '/app/index.html',
  filename: 'index.html',
  inject: 'body'
})

var IconFontPlugin = require("iconfont-loader/IconFontPlugin")

module.exports = {
  entry: ['./app/app.js'],
  output: {
    filename: 'app_bundle.js',
    path: __dirname + '/dist'
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader?presets[]=es2015&presets[]=react'
      },
      {
        test: /\.scss$/,
        exclude: /node_modules/,
        loaders: ["style", "css", "sass"]
      },
      {
        test: /\.svg$/,
        loader: "iconfont-loader"
      }
    ]
  },
  devServer: {
    contentBase: "./dist"
  },
  plugins: [
    HTMLWebpackPluginConfig,
    new IconFontPlugin({ fontName: "myfont" })
  ],
}
